# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

#DISTUTILS_USE_PEP517=hatchling
DISTUTILS_USE_PEP517=setuptools
PYTHON_COMPAT=( python3_{9..11} )

inherit distutils-r1 pypi

DESCRIPTION="A generic, modular code generation in Python 3"
HOMEPAGE="
	https://github.com/modm-io/lbuild
	https://pypi.org/project/lbuild/
"
#SRC_URI="https://github.com/modm-io/lbuild/archive/refs/heads/develop.zip"
SRC_URI="$(pypi_sdist_url)"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64"

REQUIRED_USE="${PYTHON_REQUIRED_USE}"

RDEPEND="
	dev-python/lxml
	dev-python/jinja
	dev-python/GitPython
	dev-python/anytree
	dev-python/pyelftools
"
BDEPEND="
"

distutils_enable_tests pytest

python_prepare_all() {
	distutils-r1_python_prepare_all
}

